use std::collections::HashMap;

use futures::{future, Future, Stream};

use gotham::handler::HandlerFuture;
use gotham::helpers::http::response::create_response;
use gotham::state::{FromState, State};

use hyper::Response;
use hyper::{Body, StatusCode};
use log::error;
use mime;
use url::form_urlencoded;

use crate::config::Config;
use crate::error_response::{create_html_error_response, create_text_error_response};
use crate::paste::Paste;

pub fn put(mut state: State) -> Box<HandlerFuture> {
	Box::new({
		Body::take_from(&mut state).concat2().then(|body| {
			let config = Config::take_from(&mut state);

			let res = match body {
				Ok(b) => {
					let body_content = b.into_bytes();
					let text = String::from_utf8(body_content.as_ref().to_vec()).unwrap();

					match Paste::from_text(text, config.salt) {
						Ok(paste) => {
							let mut path = config.data_directory.clone();
							path.push(paste.id.clone());

							match paste.to_file(path) {
								Ok(_) => create_response(
									&state,
									StatusCode::OK,
									mime::TEXT_PLAIN,
									format!("{}/{}\n", config.url, paste.id),
								),
								Err(e) => {
									error!("submit.rs(10): {:?}", e);
									create_text_error_response(
										StatusCode::INTERNAL_SERVER_ERROR,
										&state,
									)
								}
							}
						}
						Err(e) => {
							error!("submit.rs(20): {:?}", e);
							create_text_error_response(
								StatusCode::INTERNAL_SERVER_ERROR,
								&state,
							)
						}
					}
				}
				Err(e) => {
					error!("submit.rs(30): {:?}", e);
					create_text_error_response(StatusCode::INTERNAL_SERVER_ERROR, &state)
				}
			};

			future::ok((state, res))
		})
	})
}

pub fn post(mut state: State) -> Box<HandlerFuture> {
	Box::new({
		Body::take_from(&mut state).concat2().then(|body| {
			let config = Config::take_from(&mut state);

			let res = match body {
				Ok(b) => {
					let body_content = b.into_bytes();
					let form_map: HashMap<String, String> = form_urlencoded::parse(&body_content)
						.into_owned()
						.map(|x| x)
						.collect();

					match Paste::from_form(form_map, config.salt) {
						Ok(paste) => {
							let mut path = config.data_directory;
							path.push(paste.id.clone());

							match paste.to_file(path) {
								Ok(_) => Response::builder()
									.status(303)
									.header("Location", format!("/{}", paste.id))
									.body(Body::empty())
									.unwrap(),
								Err(e) => {
									error!("submit.rs(40): {:?}", e);
									create_html_error_response(
										StatusCode::INTERNAL_SERVER_ERROR,
										config.url,
										&state,
									)
								}
							}
						}
						Err(e) => {
							error!("submit.rs(50): {:?}", e);
							create_html_error_response(
								StatusCode::INTERNAL_SERVER_ERROR,
								config.url,
								&state,
							)
						}
					}
				}
				Err(e) => {
					error!("submit.rs(60): {:?}", e);
					create_html_error_response(StatusCode::INTERNAL_SERVER_ERROR, config.url, &state)
				}
			};

			future::ok((state, res))
		})
	})
}
