use askama::Template;
use futures::future;

use gotham::handler::HandlerFuture;
use gotham::helpers::http::response::create_response;
use gotham::state::{FromState, State};

use hyper::StatusCode;
use log::error;
use mime;

use crate::config::Config;
use crate::error_response::create_html_error_response;
use crate::syntax::SYNTAXES;

#[derive(Debug, Template)]
#[template(path = "index.html")]
pub struct Index {
	syntaxes: Vec<String>,
	site_url: String,
}

pub fn route(mut state: State) -> Box<HandlerFuture> {
	Box::new({
		let config = Config::take_from(&mut state);

		let template = Index {
			syntaxes: SYNTAXES.to_vec(),
			site_url: config.url.clone(),
		};

		let res = match template.render() {
			Ok(content) => create_response(
				&state,
				StatusCode::OK,
				mime::TEXT_HTML_UTF_8,
				content.into_bytes(),
			),
			Err(e) => {
				error!("index.rs: {:?}", e);
				create_html_error_response(StatusCode::INTERNAL_SERVER_ERROR, config.url, &state)
			}
		};

		future::ok((state, res))
	})
}
