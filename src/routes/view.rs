use askama::Template;
use futures::future;

use gotham::handler::HandlerFuture;
use gotham::helpers::http::response::create_response;
use gotham::state::{FromState, State};
use gotham_derive::{StateData, StaticResponseExtender};

use hyper::StatusCode;
use log::error;
use mime;
use serde_derive::Deserialize;

use syntect::html::ClassedHTMLGenerator;

use crate::config::Config;
use crate::error_response::create_html_error_response;
use crate::paste::Paste;
use crate::syntax::SYNTAX_SET;

#[derive(Deserialize, StateData, StaticResponseExtender)]
pub struct Params {
	id: String,
}

#[derive(Debug, Template)]
#[template(path = "view.html")]
pub struct View {
	pub id: String,
	pub dt: String,
	pub text_lines: Vec<String>,
	pub index_len: usize,
	pub site_url: String,
	pub syntax: String,
}

pub fn get(mut state: State) -> Box<HandlerFuture> {
	Box::new({
		let config = Config::take_from(&mut state);
		let Params { id } = Params::take_from(&mut state);

		let mut path = config.data_directory.clone();
		path.push(id.clone());

		let res = match Paste::from_file(path) {
			Ok(paste) => {
				let text_lines: Vec<String> = paste.text.lines().map(|s| s.into()).collect();
				let index_len = format!("{}", text_lines.len()).len();
				let syntax = SYNTAX_SET
					.find_syntax_by_name(&paste.lang)
					.unwrap_or_else(|| SYNTAX_SET.find_syntax_plain_text());
				let mut high_lines: Vec<String> = Vec::new();

				for line in text_lines {
					let mut html_generator = ClassedHTMLGenerator::new(&syntax, &SYNTAX_SET);
					html_generator.parse_html_for_line(&line);
					high_lines.push(html_generator.finalize());
				}

				let template = View {
					id,
					dt: paste.dt.format("%Y-%m-%dT%H:%MZ").to_string(),
					text_lines: high_lines,
					index_len,
					site_url: config.url.clone(),
					syntax: paste.lang,
				};

				match template.render() {
					Ok(content) => create_response(
						&state,
						StatusCode::OK,
						mime::TEXT_HTML_UTF_8,
						content.into_bytes(),
					),
					Err(e) => {
						error!("view.rs(10): {:?}", e);
						create_html_error_response(StatusCode::INTERNAL_SERVER_ERROR, config.url, &state)
					}
				}
			}
			Err(e) => {
				error!("view.rs(20): {:?}", e);
				create_html_error_response(StatusCode::INTERNAL_SERVER_ERROR, config.url, &state)
			}
		};

		future::ok((state, res))
	})
}
