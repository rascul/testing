mod config;
mod error_response;
mod logger;
mod paste;
mod result;
mod routes;
mod syntax;
#[cfg(test)]
mod tests;

use result::Result;

fn main() -> Result<()> {
	let config = config::Config::load("config.toml")?;
	logger::setup(&config.log_level)?;
	gotham::start(config.address.clone(), routes::build(config));
	Ok(())
}
